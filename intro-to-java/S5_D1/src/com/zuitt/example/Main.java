package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello World!");

        Person person1 = new Person();
        person1.run();
        person1.sleep();
        person1.rollOver();
        person1.sit();

        StaticPoly staticPoly = new StaticPoly();
        System.out.println(staticPoly.addition(1, 5));
        System.out.println(staticPoly.addition(1, 5, 10));
        System.out.println(staticPoly.addition(15.5, 15.6));

        Parent parent1 = new Parent("John", 35);
        parent1.greet();
        parent1.greet("John", "Morning");
        parent1.speak();

        Child child1 = new Child();
        child1.speak();


    }
}
