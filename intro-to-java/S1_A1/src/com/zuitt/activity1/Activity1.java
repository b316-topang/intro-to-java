package com.zuitt.activity1;
import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {
        // Variables and Initializations
        String firstName;
        String lastName;
        double firstSubject = 0;
        double secondSubject = 0;
        double thirdSubject = 0;
        int averageGrade = 0;

        Scanner myObj = new Scanner(System.in);

        // First Name
        System.out.println("What is your first name?");
        firstName = myObj.nextLine();

        // Last Name
        System.out.println("What is your last name?");
        lastName = myObj.nextLine();

        // First Subject Grade
        System.out.println("What is your first subject grade?");
        firstSubject = myObj.nextDouble();
        averageGrade += firstSubject;

        // Second Subject Grade
        System.out.println("What is your second subject grade?");
        secondSubject = myObj.nextDouble();
        averageGrade += secondSubject;

        // Third Subject Grade
        System.out.println("What is your third subject grade?");
        thirdSubject = myObj.nextDouble();
        averageGrade += thirdSubject;

        // Computing the average of the grades
        averageGrade = averageGrade/3;

        // Expected Output
            /*
                Good day, <firstName> <lastName>.
                Your grade average is: <averageGrade>
            */

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + averageGrade);

    }
}
