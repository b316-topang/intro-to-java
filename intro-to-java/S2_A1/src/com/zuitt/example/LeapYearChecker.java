package com.zuitt.example;
import java.util.Scanner;

public class LeapYearChecker {
    public static void main(String[] args) {
        int leapYear = 0;

        Scanner myObj = new Scanner(System.in);

        //Input for leap year
        System.out.println("Input year to be check if a leap year: ");
        leapYear = myObj.nextInt();

        // Checking if it's a leap year
        if ((leapYear % 4 == 0 && leapYear % 100 != 0) || leapYear % 400 == 0) {
            System.out.println(leapYear + " is a leap year.");
        } else {
            System.out.println(leapYear + " is NOT a leap year.");
        }
    }
}
