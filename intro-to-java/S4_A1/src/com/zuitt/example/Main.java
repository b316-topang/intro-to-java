package com.zuitt.example;
import java.util.Date;
import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        // Set New User
        User userA = new User();

        // Set Values
        userA.setName("Terrence Gaffud");
        userA.setAge(25);
        userA.setEmail("tgaff@mail.com");
        userA.setAddress("Quezon City");

        // Output
        userA.introduceUser();

        // Set New Course
        Course courseA = new Course();

        // Set Values
        courseA.setName("MACQ004");
        courseA.setDescription("An introduction to Java for career-shifters");
        courseA.setSeats(10);
        courseA.setFee(2000);
        courseA.setInstructor(userA);

        courseA.introduceCourse();
    }
}
