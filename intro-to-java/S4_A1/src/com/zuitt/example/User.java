package com.zuitt.example;
import java.util.Date;
import java.time.LocalDate;

public class User {
    /// Declaration and Initialization
    private String name;
    private int age;
    private String email;
    private String address;

    public User() {
        // Default constructor
    }

    public User(String name, int age, String email, String address) {
        this.name = name;
        this.age = age;
        this.email = email;
        this.address = address;
    }

    // Getter and Setters
    // Name
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }

    // Age
    public int getAge() {
        return this.age;
    }
    public void setAge(int age) {
        this.age = age;
    }

    // Email
    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    // Address
    public String getAddress() {
        return this.address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    // Methods
    public void introduceUser() {
        System.out.println("Hi! I'm " + this.name + ". I'm " + this.age + " years old. You can reach me via my email: " + this.email + ". When I'm off work, I can be found at my house in " + this.address + ".");
    }
}
