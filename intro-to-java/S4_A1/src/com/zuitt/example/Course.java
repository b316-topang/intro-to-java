package com.zuitt.example;
import java.util.Date;

public class Course extends User {
    // Declaration and Initialization
    private String name;
    private String description;
    private int seats;
    private double fee;
    private Date startDate;
    private Date endDate;
    private User instructor;

    public Course() {
        super();
        this.instructor = new User("", 0, "", "");
    }

    public Course(String name, String description, int seats, double fee, Date startDate, Date endDate, User instructor) {
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = new Date();
        this.endDate = new Date();
        this.instructor = instructor;
    }

    // Getter and Setters
    // Name
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }

    // Description
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    // Seats
    public int getSeats() {
        return this.seats;
    }
    public void setSeats(int seats) {
        this.seats = seats;
    }

    // Fee
    public double getFee() {
        return this.fee;
    }
    public void setFee(double fee) {
        this.fee = fee;
    }

    // Start Date
    public Date getStartDate() {
        return this.startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    // End Date
    public Date getEndDate() {
        return this.endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    // Instructor
    public User getInstructor() {
        return instructor;
    }

    public void setInstructor(User instructor) {
        this.instructor = instructor;
    }

    // Methods
    public void introduceCourse() {
        System.out.println("Welcome to the course " + this.getName() + ". This course can be described as " + this.description + ". Your instructor for this course is " + this.instructor.getName() + ". Enjoy!");
    }
}