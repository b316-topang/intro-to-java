package com.zuitt.example;

public class ControlStructure {
    public static void main(String[] args) {
        // if statements
        int num1 = 10;
        int num2 = 20;
        if(num1 > 5){
            System.out.println("Num1 is greater than 5");
        }

        if(num2 > 100){
            System.out.println("Num2 is greater than 100");
        } else {
            System.out.println(("Num2 is less than 100"));
        }

        // Short-circuiting
        int x = 15;
        int y = 0;
        /*if(y > 0 || x/y == 0){
            System.out.println("Result is: " + x/y);
        }*/

        // Switch Cases
        int directionValue = 4;
        switch (directionValue) {
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid");

        }
    }
}

