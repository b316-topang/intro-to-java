package com.zuitt.example;
import java.util.ArrayList;

public class ArrayListExample {
    public static void main(String[] args) {
        ArrayList<String> friends = new ArrayList<>();

        // Adding elements to the ArrayList friends
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        // Output
        System.out.println("My friends are: " + friends);
    }
}
