package com.zuitt.example;

public class PrimeNumber {
    public static void main(String[] args) {
        int[] primeNumbers = new int[5];

        // Assigning
        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        // Output
        System.out.println("The first prime number is: " + primeNumbers[0]);
        System.out.println("The second prime number is: " + primeNumbers[1]);
        System.out.println("The third prime number is: " + primeNumbers[2]);
        System.out.println("The fourth prime number is: " + primeNumbers[3]);
        System.out.println("The fifth prime number is: " + primeNumbers[4]);
    }
}
