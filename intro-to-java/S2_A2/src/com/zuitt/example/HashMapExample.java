package com.zuitt.example;
import java.util.HashMap;

public class HashMapExample {
    public static void main(String[] args) {
        HashMap<String, Integer>inventory = new HashMap<String, Integer>();

        // Adding elements into the HashMap
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        // Output
        System.out.println("Our current inventory consists of: " + inventory);
    }
}
