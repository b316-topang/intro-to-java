package com.zuitt.example;

import java.sql.SQLOutput;

public class Main {
    public static void main(String[] args) {/*
        Car car1 = new Car();
        Driver driver1 = new Driver("Alejandro", 25);

        System.out.println(car1.getCarDriver().getName());
        Driver newDriver = new Driver("Antonio", 21);

        car1.setCarDriver(newDriver);

        // Get name of New Car Driver
        System.out.println(car1.getCarDriver().getName());
        System.out.println(car1.getCarDriver().getAge());

        System.out.println(car1.getCarDriverName());*/

        Animal animal1 = new Animal("Wise", "Coffee");
        animal1.setName("Wise1");
        animal1.setColor("Caramel");
        System.out.println(animal1.getName());
        System.out.println(animal1.getColor());

        Dog dog1 = new Dog();
        System.out.println(dog1.getName());
        dog1.greet();
        dog1.setName("Hachiko");
        System.out.println(dog1.getName());

    }
}