package com.zuitt.example;

public class Animal {
    // Declarations and Initializations
    private String name;
    private String color;

    public Animal() {

    }

    public Animal(String name, String color) {
        this.name = name;
        this.color = color;
    }

    // Getter and Setters
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return this.color;
    }
    public void setColor(String color) {
        this.color = color;
    }
}
