package com.zuitt.example;

public class Dog extends Animal {
    private String dogBreed;
    public Dog() {
        super();
        this.dogBreed = "Beagle";

    }

    public Dog(String name, String color, String breed) {
        super(name, color);
        this.dogBreed = breed;
    }

    public String getDogBreed() {
        return dogBreed;
    }
    public void setDogBreed(String dogBreed) {
        this.dogBreed = dogBreed;
    }

    public void greet() {
        System.out.println("Bark!");
    }


}
