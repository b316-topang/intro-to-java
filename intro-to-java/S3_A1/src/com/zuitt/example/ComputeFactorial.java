package com.zuitt.example;
import java.sql.SQLOutput;
import java.util.Scanner;

public class ComputeFactorial {
    public static void main(String[] args) {
        // Scanner
        Scanner input = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed");

        try {

            // Declarations and Initializations
            int num = input.nextInt();
            int answer = 1;
            int counter = 1;

            if (num < 0) {
                System.out.println("Factorial is undefined for negative numbers");
            } else if (num == 0) {
                System.out.println("Factorial of 0 is 1.");
            } else {
                // While Loop
                while (counter <= num) {
                    answer *= counter;
                    counter++;
                }
                System.out.println("Factorial using while loop: " + answer);

                // Reset answer and counter
                answer = 1;
                counter = 1;

                // For Loop
                for (int i = 1; i <= num; i++) {
                    answer *= i;
                }
                System.out.println("Factorial using for loop: " + answer);
            }

        } catch (Exception e) {
            System.out.println("Invalid input! Please enter a valid integer.");
        }



    }
}
