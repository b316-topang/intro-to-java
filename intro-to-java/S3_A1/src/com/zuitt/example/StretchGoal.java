package com.zuitt.example;
import java.util.Scanner;

public class StretchGoal {
    public static void main(String[] args) {
        // Scanner
        Scanner input = new Scanner(System.in);
        System.out.println("Input how many rows: ");
        int rows = input.nextInt();

        try {
            if (rows < 0) {
                System.out.println("Please enter a positive number.");
            } else if (rows == 0) {
                System.out.println("You cannot enter 0.");
            } else {
                for(int x = 1; x <= rows; x++) {
                    for(int y = 1; y <= x; y++) {
                        System.out.print("* ");
                    }
                    System.out.println();
                }
            }
        } catch (Exception e) {
            System.out.println("Invalid input! Please enter a valid integer.");
        }

    }
}
