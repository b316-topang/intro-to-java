package com.zuitt.example;

public class Contact {
    private String name;
    private String[] contactNumber;
    private String[] address;

    public Contact() {
        // Default constructor
    }
    public Contact(String name, String[] contactNumber, String[] address) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    // Getter and Setter
    // Name
    public String getName() { return this.name; }
    public void setName(String name) { this.name = name; }

    // Contact Number
    public String[] getContactNumber() { return this.contactNumber; }
    public void setContactNumber(String[] contactNumber) { this.contactNumber = contactNumber; }

    // Address
    public String[] getAddress() { return this.address; }
    public void setAddress(String[] address){ this.address = address; }


}
