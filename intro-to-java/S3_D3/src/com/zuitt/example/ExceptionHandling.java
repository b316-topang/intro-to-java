package com.zuitt.example;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); // The scanner object looks for which input to scan. In this case, it is the console(System.in);
        /*
        * nextInt() - expects an Integer
        * nextDouble() - expects a Double
        * nextLine() - gets the entire line as a String
        */
        System.out.println("Input a number: ");
        // int num = input.nextInt(); // The nextInt() method tells Java that it is expecting an integer value as input.

        int num = 0;
        try {
            num = input.nextInt();
        } catch(Exception e) {
            System.out.println("Invalid input");
            e.printStackTrace();
        }

        System.out.println("You have entered: " + num);
    }
}
